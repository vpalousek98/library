<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\BookFacade;

class BookPresenter extends BasePresenter
{
    private BookFacade $bookFacade;

    public function __construct(BookFacade $bookFacade)
    {
        $this->bookFacade = $bookFacade;
    }

    public function renderDefault(): void
    {
        $this->template->books = $this->bookFacade->getBooks()->fetchAll();
    }
}
