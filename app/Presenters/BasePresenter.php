<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\BookFacade;
use Nette;


class BasePresenter extends Nette\Application\UI\Presenter
{
    private BookFacade $bookFacade;

    public function injectBookFacade(BookFacade $bookFacade): void
    {
        $this->bookFacade = $bookFacade;
    }

    public function handleSaveBook($name, $author, $release_year): void
    {
        $this->bookFacade->save($name, $author, $release_year);

        $this->redrawControl('booksList');
        $this->redrawControl('authorBooksList');
    }
}
