<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\AuthorFacade;

final class AuthorPresenter extends BasePresenter
{
    private AuthorFacade $authorFacade;

    public function __construct(AuthorFacade $authorFacade)
    {
        $this->authorFacade = $authorFacade;
    }

    public function renderDetail(int $id): void
    {
        $author = $this->authorFacade->findAuthorById($id)->fetch();

        if ($author === null) {
            $this->redirect('Book:default');
        }

        $this->template->author = $author;
        $this->template->authorBooks = $author->related('book');
    }

    public function actionAutocomplete(string $q = ''): void
    {
        $this->sendJson(array_values(array_map(function ($author) {
            return [
                'value' => $author->id,
                'text' => $author->first_name . ' ' . $author->last_name,
            ];
        }, $this->authorFacade->autocomplete($q)->fetchAll())));
    }
}
