<?php

namespace App\Model;

use Nette;

final class AuthorFacade
{
    const TABLE = 'author';

    use Nette\SmartObject;

    private Nette\Database\Explorer $database;

    public function __construct(Nette\Database\Explorer $database)
    {
        $this->database = $database;
    }

    public function findAuthors(): Nette\Database\Table\Selection
    {
        return $this->database
            ->table(self::TABLE);
    }

    public function findAuthorById(int $id): Nette\Database\Table\Selection
    {
        return $this->database
            ->table(self::TABLE)
            ->where('id = ?' , $id);
    }

    public function autocomplete(string $q): Nette\Database\Table\Selection
    {
        return $this->database
            ->table(self::TABLE)
            ->whereOr([
                'first_name LIKE ?' => "%$q%",
                'last_name LIKE ?' => "%$q%",
            ]);
    }
}