<?php

namespace App\Model;

use Nette;

final class BookFacade
{
    const TABLE = 'book';

    use Nette\SmartObject;

    private Nette\Database\Explorer $database;

    public function __construct(Nette\Database\Explorer $database)
    {
        $this->database = $database;
    }

    public function getBooks(): Nette\Database\Table\Selection
    {
        return $this->database
            ->table(self::TABLE)
            ->order('created_at DESC');
    }

    public function getBook(int $id): Nette\Database\Table\Selection
    {
        return $this->database
            ->table(self::TABLE)
            ->where([
                'id' => $id
            ]);
    }

    public function save($name, $authorId, $releaseYear): Nette\Database\ResultSet
    {
        return $this->database->query('INSERT INTO ' . self::TABLE, [
            'created_at' => new Nette\Utils\DateTime(),
            'name' => $name,
            'author_id' => $authorId,
            'release_year' => $releaseYear
        ]);
    }
}